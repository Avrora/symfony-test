<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\Common\Persistence\ObjectManager;
use App\DataFixtures\DogFixture;
use App\Entity\Dog;

class DogControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * La méthode setUp sera lancée avant chacune des méthodes de test,
     * elle sert à mettre en place l'environnement des tests de la
     * classe actuelle.
     * On s'en sert ici pour créer nos tables SQL
     */
    public function setUp() {
        //On crée le client dans le setUp vu qu'on s'en servira dans 
        //chacune de nos méthodes
        $this->client = static::createClient();
        //On crée un ObjectManager avec ce client
        $manager = $this->client->getContainer()->get('doctrine')->getManager();
        //On crée un SchemaTool qui servira à manipuler la structure de
        //la base de données
        $schemaTool = new SchemaTool($manager);
        //On chope les métadonnées de nos entités
        $classes = $manager->getMetadataFactory()->getAllMetadata();
        //On supprime les tables existantes de la bdd
        $schemaTool->dropSchema($classes);
        //On (re)crée les tables dans la bdd
        $schemaTool->createSchema($classes);

        $fixtures = new DogFixture(); 
        $fixtures->load($manager);
    }

    public function testShowDogs()
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    
        $li = $crawler->filter('li');
        $this->assertCount(5, $li);
        $this->assertContains('Dog 1', $li->text());
        $this->assertContains('Breed 1', $li->text());
    }

    public function testAddDogSuccess() {
        $crawler = $this->client->request('GET', '/add-dog');
        
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        //avec un filter c'est bon aussi
        $form = $crawler->selectButton('Add')->form();

        $form['dog[name]'] = 'fromtest';
        $form['dog[breed]'] = 'fromtest';
        $form['dog[age]'] = 1;

        $this->client->submit($form);

        // $this->client->submitForm('Add', [
        //     'dog[name]' => 'fromtest',
        //     'dog[breed]' => 'fromtest',
        //     'dog[age]' => 1,
        // ]);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $repo = $this->client->getContainer()->get('doctrine')->getRepository(Dog::class);

        $this->assertSame(6, $repo->count([]));


    }

}