<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DogRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\DogType;
use App\Entity\Dog;

class DogController extends AbstractController
{
    /**
     * @Route("/", name="dog")
     */
    public function index(DogRepository $repo)
    {
        return $this->render('dog/index.html.twig', [
            'dogList' => $repo->findAll(),
        ]);
    }

   /**
     * @Route("/add-dog", name="add_dog")
     */
    public function add(ObjectManager $manager, Request $request) {
        $dog = new Dog();
        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($dog);
            $manager->flush();
        }
        return $this->render('dog/add.html.twig', [
            'form' => $form->createView()
        ]);
    }



}
