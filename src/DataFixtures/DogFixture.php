<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Dog;


class DogFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // create 5 dogs
        for ($i = 1; $i < 6; $i++) {
            $dog = new Dog();
            $dog->setName('Dog ' . $i);
            $dog->setBreed('Breed ' . $i);
            $dog->setAge(12 + $i);
            $manager->persist($dog);
        }

        $manager->flush();
    }
}